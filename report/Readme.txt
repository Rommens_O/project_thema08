README Theme 08: System Biology
Olaf Rommens, Joas Weeda
371335, 380897
BFV2
14-6-2019

### Folders and files
Article
contents:
- link to the article used in this project
- pdf-file from the article by van Zyl, used in this project

Endreport
contents:
- final project in the form of an Rmd- and pdf-file

Markdown
contents:
- Rmd-file that contains the function used in the simulation 
- simulation used to make the plots 

PNG_files
contents:
- png-files used in the Rmd file to display in the final pdf-file
